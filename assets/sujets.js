require('datatables.net-bs5');
import $ from 'jquery';

(() => {
    let httpRequest;

    let start_measure;

    function makeRequest(id){
        start_measure = Date.now();

        httpRequest = new XMLHttpRequest();

        if (!httpRequest) {
            alert("Giving up :(Cannot create an XMLHTTP instance");
            return false;
        }
        httpRequest.onreadystatechange = updateData;

        httpRequest.open("GET","https://localhost:8000/sujet/api/"+id);
        httpRequest.send();
    }

    function updateData(){
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {

                const data = JSON.parse(httpRequest.responseText);

                console.log(data);

                $('#modal-title').text(data.titre);
                $('#modal-respos').text(data.respos);
                $('#modal-duree').text(data.duree);
                $('#modal-places').text(data.places);
                $('#modal-desc').text(data.description);
                if (data.places > 0){
                    $('#modal-contact').attr('href', 'https://localhost:8000/contact/'+data.id)
                        .attr('target', '_blank')
                        .attr('rel', 'noreferrer noopener').removeClass("disabled")
                    ;
                } else {
                    $('#modal-contact-wrap').attr('title','Ce stage est complet!');
                }

                const end_measure = Date.now();
                console.log(`Execution time: ${end_measure - start_measure} ms`);

            } else {
                alert("There was a problem with the request.");
            }
        }
    }

    window.makeRequest = makeRequest;
})()
$(document).ready(function() {

    $('#example').DataTable();

    const myModalEl = document.getElementById('exampleModal')
    myModalEl.addEventListener('hidden.bs.modal', event => {
        // do something...
        $('#modal-title').text("Chargement...");
        $('#modal-respos').text("Chargement...");
        $('#modal-duree').text("Chargement...");
        $('#modal-places').text("Chargement...");
        $('#modal-desc').text("Chargement...");
        $('#modal-contact').addClass('disabled');
        $('#modal-contact-wrap').removeAttr('title');
    })

    // ========================================================================================================================
    //          Exemples de manipulation des DataTables
    // ========================================================================================================================
    // const basicColumns = [
    //     { title: "Titre"}, { title: "Responsable(s)"}, { title: "Durée Prévue"}, { title: "Place(s) restante(s)"}
    // ]
    //
    // const basicData = [
    //     [formatDataTitle("Ensemble runs on Supercomputers: Towards Energy Efficiency"), "Bruno Raffin (LIG-INRIA-MOAIS)", "4 mois", "1/1"],
    //     [formatDataTitle("Detecting Tortured Phrases in Scientific Literature"), "Cyril Labbe (LIG-SIGMA)", "3 mois", "0/1"],
    //     [formatDataTitle("Natural Language Understanding to Fact Checking Nucleotide Sequences in Life Science Publications."), "Cyril Labbe (LIG-SIGMA)", "6 mois", "3/3"]
    // ]
    //
    // let table = $('#example').DataTable({data: basicData, columns: basicColumns});
    // new DataTable(document.getElementById("example"), basicData);

    // table.on('click', 'tbody tr', function () {
    //     let data = table.row(this).data();
    //
    //     alert('You clicked on ' + data[0] + "'s row");
    // });
    // ========================================================================================================================

});

window.test = test;
