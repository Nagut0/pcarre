<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230812120618 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE annee_scolaire (id INT AUTO_INCREMENT NOT NULL, annee INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filiere (id INT AUTO_INCREMENT NOT NULL, nom_fil VARCHAR(254) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filiere_sujet (filiere_id INT NOT NULL, sujet_id INT NOT NULL, INDEX IDX_57431B3B180AA129 (filiere_id), INDEX IDX_57431B3B7C4D497E (sujet_id), PRIMARY KEY(filiere_id, sujet_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE infos_resp (id INT AUTO_INCREMENT NOT NULL, prenom VARCHAR(254) NOT NULL, nom VARCHAR(254) NOT NULL, email VARCHAR(254) NOT NULL, url VARCHAR(254) DEFAULT NULL, telephone VARCHAR(254) DEFAULT NULL, login VARCHAR(254) NOT NULL, uid VARCHAR(254) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inscription (id INT AUTO_INCREMENT NOT NULL, sujet_id INT DEFAULT NULL, prenom VARCHAR(254) NOT NULL, nom VARCHAR(254) NOT NULL, email VARCHAR(254) NOT NULL, date_insc DATETIME NOT NULL, etablissement VARCHAR(254) DEFAULT NULL, annotation VARCHAR(254) DEFAULT NULL, INDEX IDX_5E90F6D67C4D497E (sujet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE laboratoire (id INT AUTO_INCREMENT NOT NULL, nom_lab VARCHAR(254) NOT NULL, rue_lab VARCHAR(254) NOT NULL, ville_lab VARCHAR(254) NOT NULL, cp_lab VARCHAR(254) NOT NULL, site_lab VARCHAR(254) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mot_cle (id INT AUTO_INCREMENT NOT NULL, contenu VARCHAR(254) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mot_cle_sujet (mot_cle_id INT NOT NULL, sujet_id INT NOT NULL, INDEX IDX_34448C1AFE94535C (mot_cle_id), INDEX IDX_34448C1A7C4D497E (sujet_id), PRIMARY KEY(mot_cle_id, sujet_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parametre_appli (id INT AUTO_INCREMENT NOT NULL, nom_param VARCHAR(254) NOT NULL, valeur_param VARCHAR(254) NOT NULL, type_param VARCHAR(254) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE responsable (id INT AUTO_INCREMENT NOT NULL, infos_id INT DEFAULT NULL, labo_id INT DEFAULT NULL, INDEX IDX_52520D07544A4CCA (infos_id), INDEX IDX_52520D07B65FA4A (labo_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE responsable_sujet (responsable_id INT NOT NULL, sujet_id INT NOT NULL, INDEX IDX_4F827CFF53C59D72 (responsable_id), INDEX IDX_4F827CFF7C4D497E (sujet_id), PRIMARY KEY(responsable_id, sujet_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sujet (id INT AUTO_INCREMENT NOT NULL, annee_id INT DEFAULT NULL, titre_sujet VARCHAR(254) NOT NULL, nb_max_etu INT NOT NULL, date_creation DATETIME NOT NULL, description VARCHAR(254) DEFAULT NULL, duree_semaines INT DEFAULT NULL, INDEX IDX_2E13599D543EC5F0 (annee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE filiere_sujet ADD CONSTRAINT FK_57431B3B180AA129 FOREIGN KEY (filiere_id) REFERENCES filiere (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filiere_sujet ADD CONSTRAINT FK_57431B3B7C4D497E FOREIGN KEY (sujet_id) REFERENCES sujet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE inscription ADD CONSTRAINT FK_5E90F6D67C4D497E FOREIGN KEY (sujet_id) REFERENCES sujet (id)');
        $this->addSql('ALTER TABLE mot_cle_sujet ADD CONSTRAINT FK_34448C1AFE94535C FOREIGN KEY (mot_cle_id) REFERENCES mot_cle (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mot_cle_sujet ADD CONSTRAINT FK_34448C1A7C4D497E FOREIGN KEY (sujet_id) REFERENCES sujet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE responsable ADD CONSTRAINT FK_52520D07544A4CCA FOREIGN KEY (infos_id) REFERENCES infos_resp (id)');
        $this->addSql('ALTER TABLE responsable ADD CONSTRAINT FK_52520D07B65FA4A FOREIGN KEY (labo_id) REFERENCES laboratoire (id)');
        $this->addSql('ALTER TABLE responsable_sujet ADD CONSTRAINT FK_4F827CFF53C59D72 FOREIGN KEY (responsable_id) REFERENCES responsable (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE responsable_sujet ADD CONSTRAINT FK_4F827CFF7C4D497E FOREIGN KEY (sujet_id) REFERENCES sujet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sujet ADD CONSTRAINT FK_2E13599D543EC5F0 FOREIGN KEY (annee_id) REFERENCES annee_scolaire (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE filiere_sujet DROP FOREIGN KEY FK_57431B3B180AA129');
        $this->addSql('ALTER TABLE filiere_sujet DROP FOREIGN KEY FK_57431B3B7C4D497E');
        $this->addSql('ALTER TABLE inscription DROP FOREIGN KEY FK_5E90F6D67C4D497E');
        $this->addSql('ALTER TABLE mot_cle_sujet DROP FOREIGN KEY FK_34448C1AFE94535C');
        $this->addSql('ALTER TABLE mot_cle_sujet DROP FOREIGN KEY FK_34448C1A7C4D497E');
        $this->addSql('ALTER TABLE responsable DROP FOREIGN KEY FK_52520D07544A4CCA');
        $this->addSql('ALTER TABLE responsable DROP FOREIGN KEY FK_52520D07B65FA4A');
        $this->addSql('ALTER TABLE responsable_sujet DROP FOREIGN KEY FK_4F827CFF53C59D72');
        $this->addSql('ALTER TABLE responsable_sujet DROP FOREIGN KEY FK_4F827CFF7C4D497E');
        $this->addSql('ALTER TABLE sujet DROP FOREIGN KEY FK_2E13599D543EC5F0');
        $this->addSql('DROP TABLE annee_scolaire');
        $this->addSql('DROP TABLE filiere');
        $this->addSql('DROP TABLE filiere_sujet');
        $this->addSql('DROP TABLE infos_resp');
        $this->addSql('DROP TABLE inscription');
        $this->addSql('DROP TABLE laboratoire');
        $this->addSql('DROP TABLE mot_cle');
        $this->addSql('DROP TABLE mot_cle_sujet');
        $this->addSql('DROP TABLE parametre_appli');
        $this->addSql('DROP TABLE responsable');
        $this->addSql('DROP TABLE responsable_sujet');
        $this->addSql('DROP TABLE sujet');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
