<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230824121636 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_8D93D649539B0606 ON user');
        $this->addSql('ALTER TABLE user ADD uid_number VARCHAR(180) NOT NULL, ADD given_name VARCHAR(254) NOT NULL, ADD sn VARCHAR(254) NOT NULL, ADD mail VARCHAR(254) NOT NULL, CHANGE uid uid VARCHAR(50) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649A54498BD ON user (uid_number)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_8D93D649A54498BD ON user');
        $this->addSql('ALTER TABLE user DROP uid_number, DROP given_name, DROP sn, DROP mail, CHANGE uid uid VARCHAR(180) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649539B0606 ON user (uid)');
    }
}
