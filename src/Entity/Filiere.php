<?php

namespace App\Entity;

use App\Repository\FiliereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FiliereRepository::class)]
class Filiere
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254, unique: true)]
    private ?string $nom_fil = null;

    #[ORM\ManyToMany(targetEntity: Sujet::class, inversedBy: 'filieres')]
    private Collection $sujets;

    public function __construct()
    {
        $this->sujets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFil(): ?string
    {
        return $this->nom_fil;
    }

    public function setNomFil(string $nom_fil): static
    {
        $this->nom_fil = $nom_fil;

        return $this;
    }

    /**
     * @return Collection<int, Sujet>
     */
    public function getSujets(): Collection
    {
        return $this->sujets;
    }

    public function addSujet(Sujet $sujet): static
    {
        if (!$this->sujets->contains($sujet)) {
            $this->sujets->add($sujet);
        }

        return $this;
    }

    public function removeSujet(Sujet $sujet): static
    {
        $this->sujets->removeElement($sujet);

        return $this;
    }

    public function __toString(): string{
        return $this->nom_fil;
    }
}
