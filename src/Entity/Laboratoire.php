<?php

namespace App\Entity;

use App\Repository\LaboratoireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LaboratoireRepository::class)]
class Laboratoire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254)]
    private ?string $nom_lab = null;

    #[ORM\Column(length: 254)]
    private ?string $rue_lab = null;

    #[ORM\Column(length: 254)]
    private ?string $ville_lab = null;

    #[ORM\Column(length: 254)]
    private ?string $cp_lab = null;

    #[ORM\Column(length: 254, nullable: true)]
    private ?string $site_lab = null;

    #[ORM\OneToMany(mappedBy: 'labo', targetEntity: Responsable::class)]
    private Collection $responsables;

    public function __construct()
    {
        $this->responsables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomLab(): ?string
    {
        return $this->nom_lab;
    }

    public function setNomLab(string $nom_lab): static
    {
        $this->nom_lab = $nom_lab;

        return $this;
    }

    public function getRueLab(): ?string
    {
        return $this->rue_lab;
    }

    public function setRueLab(string $rue_lab): static
    {
        $this->rue_lab = $rue_lab;

        return $this;
    }

    public function getVilleLab(): ?string
    {
        return $this->ville_lab;
    }

    public function setVilleLab(string $ville_lab): static
    {
        $this->ville_lab = $ville_lab;

        return $this;
    }

    public function getCpLab(): ?string
    {
        return $this->cp_lab;
    }

    public function setCpLab(string $cp_lab): static
    {
        $this->cp_lab = $cp_lab;

        return $this;
    }

    public function getSiteLab(): ?string
    {
        return $this->site_lab;
    }

    public function setSiteLab(?string $site_lab): static
    {
        $this->site_lab = $site_lab;

        return $this;
    }

    /**
     * @return Collection<int, Responsable>
     */
    public function getResponsables(): Collection
    {
        return $this->responsables;
    }

    public function addResponsable(Responsable $responsable): static
    {
        if (!$this->responsables->contains($responsable)) {
            $this->responsables->add($responsable);
            $responsable->setLabo($this);
        }

        return $this;
    }

    public function removeResponsable(Responsable $responsable): static
    {
        if ($this->responsables->removeElement($responsable)) {
            // set the owning side to null (unless already changed)
            if ($responsable->getLabo() === $this) {
                $responsable->setLabo(null);
            }
        }

        return $this;
    }
}
