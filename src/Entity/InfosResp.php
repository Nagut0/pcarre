<?php

namespace App\Entity;

use App\Repository\InfosRespRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InfosRespRepository::class)]
class InfosResp
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254)]
    private ?string $prenom = null;

    #[ORM\Column(length: 254)]
    private ?string $nom = null;

    #[ORM\Column(length: 254)]
    private ?string $email = null;

    #[ORM\Column(length: 254, nullable: true)]
    private ?string $url = null;

    #[ORM\Column(length: 254, nullable: true)]
    private ?string $telephone = null;

    #[ORM\Column(length: 254)]
    # Correspond à l'uid d'Agalan
    private ?string $login = null;

    #[ORM\Column(length: 254, unique:true)]
    # Correspond à l'uidNumber d'Agalan
    private ?string $uid = null;

    #[ORM\OneToMany(mappedBy: 'infos', targetEntity: Responsable::class)]
    private Collection $responsables;

    public function __construct()
    {
        $this->responsables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): static
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): static
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): static
    {
        $this->login = $login;

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): static
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * @return Collection<int, Responsable>
     */
    public function getResponsables(): Collection
    {
        return $this->responsables;
    }

    public function addResponsable(Responsable $responsable): static
    {
        if (!$this->responsables->contains($responsable)) {
            $this->responsables->add($responsable);
            $responsable->setInfos($this);
        }

        return $this;
    }

    public function removeResponsable(Responsable $responsable): static
    {
        if ($this->responsables->removeElement($responsable)) {
            // set the owning side to null (unless already changed)
            if ($responsable->getInfos() === $this) {
                $responsable->setInfos(null);
            }
        }

        return $this;
    }
}
