<?php

namespace App\Entity;

use App\Repository\ParametreAppliRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ParametreAppliRepository::class)]
class ParametreAppli
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254)]
    private ?string $nom_param = null;

    #[ORM\Column(length: 254)]
    private ?string $valeur_param = null;

    #[ORM\Column(length: 254)]
    private ?string $type_param = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomParam(): ?string
    {
        return $this->nom_param;
    }

    public function setNomParam(string $nom_param): static
    {
        $this->nom_param = $nom_param;

        return $this;
    }

    public function getValeurParam(): ?string
    {
        return $this->valeur_param;
    }

    public function setValeurParam(string $valeur_param): static
    {
        $this->valeur_param = $valeur_param;

        return $this;
    }

    public function getTypeParam(): ?string
    {
        return $this->type_param;
    }

    public function setTypeParam(string $type_param): static
    {
        $this->type_param = $type_param;

        return $this;
    }
}
