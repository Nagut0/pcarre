<?php

namespace App\Entity;

use App\Repository\SujetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SujetRepository::class)]
class Sujet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 254)]
    private ?string $titre_sujet = null;

    #[ORM\Column]
    private ?int $nb_max_etu = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date_creation = null;

    #[ORM\Column(length: 254, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?int $duree_semaines = null;

    #[ORM\ManyToMany(targetEntity: Responsable::class, mappedBy: 'sujets')]
    private Collection $responsables;

    #[ORM\OneToMany(mappedBy: 'sujet', targetEntity: Inscription::class)]
    private Collection $inscriptions;

    #[ORM\ManyToMany(targetEntity: MotCle::class, mappedBy: 'sujets', fetch: "EAGER")]
    private Collection $motCles;

    #[ORM\ManyToMany(targetEntity: Filiere::class, mappedBy: 'sujets', fetch: "EAGER")]
    private Collection $filieres;

    #[ORM\ManyToOne(fetch: "EAGER", inversedBy: 'sujets')]
    private ?AnneeScolaire $annee = null;

    public function __construct()
    {
        $this->responsables = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
        $this->motCles = new ArrayCollection();
        $this->filieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreSujet(): ?string
    {
        return $this->titre_sujet;
    }

    public function setTitreSujet(string $titre_sujet): static
    {
        $this->titre_sujet = $titre_sujet;

        return $this;
    }

    public function getNbMaxEtu(): ?int
    {
        return $this->nb_max_etu;
    }

    public function setNbMaxEtu(int $nb_max_etu): static
    {
        $this->nb_max_etu = $nb_max_etu;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(\DateTimeInterface $date_creation): static
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getDureeSemaines(): ?int
    {
        return $this->duree_semaines;
    }

    public function setDureeSemaines(?int $duree_semaines): static
    {
        $this->duree_semaines = $duree_semaines;

        return $this;
    }

    /**
     * @return Collection<int, Responsable>
     */
    public function getResponsables(): Collection
    {
        return $this->responsables;
    }

    public function addResponsable(Responsable $responsable): static
    {
        if (!$this->responsables->contains($responsable)) {
            $this->responsables->add($responsable);
            $responsable->addSujet($this);
        }

        return $this;
    }

    public function removeResponsable(Responsable $responsable): static
    {
        if ($this->responsables->removeElement($responsable)) {
            $responsable->removeSujet($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Inscription>
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): static
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions->add($inscription);
            $inscription->setSujet($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): static
    {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getSujet() === $this) {
                $inscription->setSujet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MotCle>
     */
    public function getMotCles(): Collection
    {
        return $this->motCles;
    }

    public function addMotCle(MotCle $motCle): static
    {
        if (!$this->motCles->contains($motCle)) {
            $this->motCles->add($motCle);
            $motCle->addSujet($this);
        }

        return $this;
    }

    public function removeMotCle(MotCle $motCle): static
    {
        if ($this->motCles->removeElement($motCle)) {
            $motCle->removeSujet($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Filiere>
     */
    public function getFilieres(): Collection
    {
        return $this->filieres;
    }

    public function addFiliere(Filiere $filiere): static
    {
        if (!$this->filieres->contains($filiere)) {
            $this->filieres->add($filiere);
            $filiere->addSujet($this);
        }

        return $this;
    }

    public function removeFiliere(Filiere $filiere): static
    {
        if ($this->filieres->removeElement($filiere)) {
            $filiere->removeSujet($this);
        }

        return $this;
    }

    public function getAnnee(): ?AnneeScolaire
    {
        return $this->annee;
    }

    public function setAnnee(?AnneeScolaire $annee): static
    {
        $this->annee = $annee;

        return $this;
    }

    public function __toString(): string
    {
        return $this->titre_sujet;
    }
}
