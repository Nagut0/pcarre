<?php

namespace App\Entity;

use App\Repository\ResponsableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ResponsableRepository::class)]
class Responsable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(fetch: "EAGER", inversedBy: 'responsables')]
    private ?InfosResp $infos = null;

    #[ORM\ManyToOne(inversedBy: 'responsables')]
    private ?Laboratoire $labo = null;

    #[ORM\ManyToMany(targetEntity: Sujet::class, inversedBy: 'responsables')]
    private Collection $sujets;

    public function __construct()
    {
        $this->sujets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInfos(): ?InfosResp
    {
        return $this->infos;
    }

    public function setInfos(?InfosResp $infos): static
    {
        $this->infos = $infos;

        return $this;
    }

    public function getLabo(): ?Laboratoire
    {
        return $this->labo;
    }

    public function setLabo(?Laboratoire $labo): static
    {
        $this->labo = $labo;

        return $this;
    }

    /**
     * @return Collection<int, Sujet>
     */
    public function getSujets(): Collection
    {
        return $this->sujets;
    }

    public function addSujet(Sujet $sujet): static
    {
        if (!$this->sujets->contains($sujet)) {
            $this->sujets->add($sujet);
        }

        return $this;
    }

    public function removeSujet(Sujet $sujet): static
    {
        $this->sujets->removeElement($sujet);

        return $this;
    }

    public function __toString(): string
    {
        return $this->infos->getPrenom().' '.$this->infos->getNom();
    }
}
