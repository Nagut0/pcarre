<?php

namespace App\DataFixtures;

use App\Entity\AnneeScolaire;
use App\Entity\Filiere;
use App\Entity\MotCle;
use App\Entity\Responsable;
use App\Entity\Sujet;
use App\Repository\AnneeScolaireRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SujetFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $sujet = new Sujet();

            $sujet->setTitreSujet($faker->sentence());
            $sujet->setNbMaxEtu($faker->numberBetween(1,4));
            $sujet->setDateCreation(new \DateTime($faker->date()));
            $sujet->setDescription($faker->paragraph(2));
            $sujet->setDureeSemaines($faker->numberBetween(1,24));
            $sujet->setAnnee($this->doctrine->getRepository(AnneeScolaire::class)->randRow());

            $sujet->addResponsable($this->doctrine->getRepository(Responsable::class)->randRow());
            if(($i % 5) == 0){
                $sujet->addResponsable($this->doctrine->getRepository(Responsable::class)->randRow());
            }
            $sujet->addFiliere($this->doctrine->getRepository(Filiere::class)->randRow());
            $sujet->addMotCle($this->doctrine->getRepository(MotCle::class)->randRow());

            $manager->persist($sujet);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AnneeScolaireFixtures::class,
            ResponsableFixtures::class,
            FiliereFixtures::class,
            MotCleFixtures::class
        ];
    }
}
