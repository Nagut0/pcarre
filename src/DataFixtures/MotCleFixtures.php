<?php

namespace App\DataFixtures;

use App\Entity\MotCle;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class MotCleFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $mot = new MotCle();

            $mot->setContenu($faker->word().$i);

            $manager->persist($mot);
        }

        $manager->flush();
    }
}
