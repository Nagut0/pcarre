<?php

namespace App\DataFixtures;

use App\Entity\InfosResp;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class InfosRespFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $userRepo = $this->doctrine->getRepository(User::class);

        for ($i=0; $i < 20; $i++){
            $user = $userRepo->findBy(['uidNumber' => "uidNumber$i"])[0];
            if($user instanceof User){
                $info = new InfosResp();
                $info->setPrenom($user->getGivenName());
                $info->setNom($user->getSn());
                $info->setEmail($user->getMail());
                $info->setUrl($faker->url());
                $info->setTelephone($faker->phoneNumber());
                $info->setLogin($user->getUid());
                $info->setUid($user->getUidNumber());

                $manager->persist($info);
            }
        }

            $manager->flush();
    }
    public function getDependencies()
    {
        return [
            UserFixture::class
        ];
    }

}
