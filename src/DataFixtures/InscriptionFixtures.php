<?php

namespace App\DataFixtures;

use App\Entity\Inscription;
use App\Entity\Sujet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class InscriptionFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $insc = new Inscription();

            $insc->setPrenom($faker->firstName());
            $insc->setNom($faker->lastName());

            # Insérer e-mail de destination des tests
            $insc->setEmail("");

            $insc->setDateInsc(new \DateTime($faker->date()));
            $insc->setEtablissement("Etablissement$i");
            $insc->setAnnotation($faker->paragraph());
            $insc->setSujet($this->doctrine->getRepository(Sujet::class)->randRow());

            $manager->persist($insc);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SujetFixtures::class
        ];
    }
}
