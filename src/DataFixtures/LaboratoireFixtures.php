<?php

namespace App\DataFixtures;

use App\Entity\Laboratoire;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class LaboratoireFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 20; $i++){
            $labo = new Laboratoire();
            $labo->setNomLab("Laboratoire$i");
            $labo->setRueLab($faker->streetAddress());
            $labo->setVilleLab("Ville$i");
            $labo->setCpLab($faker->postcode());
            $labo->setSiteLab($faker->url());

            $manager->persist($labo);
        }

            $manager->flush();
    }
}
