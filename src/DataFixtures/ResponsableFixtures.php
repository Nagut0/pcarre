<?php

namespace App\DataFixtures;

use App\Entity\InfosResp;
use App\Entity\Laboratoire;
use App\Entity\Responsable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

class ResponsableFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    public function load(ObjectManager $manager): void
    {
        for ($i=0; $i < 20; $i++){
            $respo = new Responsable();

            $respo->setInfos($this->doctrine->getRepository(InfosResp::class)->randRow());
            $respo->setLabo($this->doctrine->getRepository(Laboratoire::class)->randRow());

            $manager->persist($respo);
        }

            $manager->flush();
    }

    public function getDependencies()
    {
        return [
            InfosRespFixtures::class,
            LaboratoireFixtures::class
        ];
    }
}
