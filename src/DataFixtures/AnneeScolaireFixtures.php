<?php

namespace App\DataFixtures;

use App\Entity\AnneeScolaire;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AnneeScolaireFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i=0; $i < 24; $i++){
            $annee = new AnneeScolaire();
            $annee->setAnnee(2000+$i);

            $manager->persist($annee);
        }

        $manager->flush();
    }
}
