<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixture extends Fixture implements FixtureGroupInterface
{
    public function __construct(
        private UserPasswordHasherInterface $hasher
    ) {}

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $admin1 = new User();
        $admin1->setMail('admin@gmail.com');
        $admin1->setPassword($this->hasher->hashPassword($admin1,'admin'));
        $admin1->setRoles(['ROLE_ADMIN']);
        $admin1->setUidNumber('uidNumber0');
        $admin1->setUid('username0');
        $admin1->setGivenName($faker->firstName());
        $admin1->setSn($faker->lastName());

        $manager->persist($admin1);

        $admin2 = new User();
        $admin2->setMail('admin2@gmail.com');
        $admin2->setPassword($this->hasher->hashPassword($admin2, 'admin'));
        $admin2->setRoles(['ROLE_ADMIN']);
        $admin2->setUidNumber('uidNumber1');
        $admin2->setUid('username1');
        $admin2->setGivenName($faker->firstName());
        $admin2->setSn($faker->lastName());

        $manager->persist($admin2);

        for ($i=2; $i < 20; $i++) {
            $user = new User();

            # Insérer mail de destination pour les tests
            $user->setMail("");

            $user->setPassword($this->hasher->hashPassword($user,'enseignant'));
            $user->setRoles(['ROLE_ENSEIGNANT']);
            $user->setUidNumber("uidNumber$i");
            $user->setUid("username$i");
            $user->setGivenName($faker->firstName());
            $user->setSn($faker->lastName());

            $manager->persist($user);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['user'];
    }
}
