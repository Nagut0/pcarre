<?php

namespace App\DataFixtures;

use App\Entity\Filiere;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FiliereFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i=0; $i < 20; $i++){
            $filiere = new Filiere();

            $filiere->setNomFil("Filiere$i");

            $manager->persist($filiere);
        }

        $manager->flush();
    }
}
