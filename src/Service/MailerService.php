<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerService
{
    public function __construct(private MailerInterface $mailer)
    {
    }

    # Envoie un mail avec le template d'inscription à l'adresse passée en paramètre
    # Utilise le contexte pour initialiser les variables du template
    public function sendInscConfirmationMail(
        Address $to,
        $context = []
    ): void
    {
        $email = (new TemplatedEmail())
            ->from('fabien@example.com')
            ->to($to )
            ->subject('Candidature de stage')

            // path of the Twig template to render
            ->htmlTemplate('emails/inscConfirmation_mail.html.twig')

            // pass variables (name => value) to the template
            ->context($context)
        ;

        $this->mailer->send($email);
    }

    # Envoie un mail avec le template de contact à l'adresse passée en paramètre
    # Utilise le contexte pour initialiser les variables du template
    public function sendContactMail(
        Address $to,
        $context = []
    ): void
    {
        $email = (new TemplatedEmail())
            ->from('fabien@example.com')
            ->to($to)
            ->subject('Candidature de stage')

            // path of the Twig template to render
            ->htmlTemplate('emails/contact_mail.html.twig')

            // pass variables (name => value) to the template
            ->context($context)
        ;

        $this->mailer->send($email);
    }
}