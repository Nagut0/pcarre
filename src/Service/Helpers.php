<?php

namespace App\Service;

use App\Entity\InfosResp;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

class Helpers
{
    public function __construct(private ManagerRegistry $doctrine)
    {
    }

    # Renvoie l'année scolaire (en string) à partir de l'année passée en paramètres
    public function getScholarFromYear(int $year): string{
        return $year.'-'.($year+1);
    }

    # Renvoie l'année de début de l'année scolaire passée en paramètre
    public function getYearFromScholar(string $year): int{
        return explode('-', $year)[0];
    }

    # Récupère l'infosResp à partir du User
    public function getInfosRespFromUser(User $user): InfosResp|null{
        return $this->doctrine->getRepository(InfosResp::class)->findBy(['uid' => $user->getUidNumber()])[0];
    }
}