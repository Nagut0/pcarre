<?php

namespace App\Controller;

use App\Entity\InfosResp;
use App\Entity\Inscription;
use App\Entity\Sujet;
use App\Entity\User;
use App\Form\InscriptionType;
use App\Service\Helpers;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class InscriptionController extends AbstractController
{
    #[
        Route('/inscription/delete/{id}', name: 'app_inscription_delete'),
        IsGranted('ROLE_ENSEIGNANT')
    ]
    public function delete(Request $request, ManagerRegistry $doctrine, Inscription $inscription = null): RedirectResponse
    {
        if ($inscription) {
            # Suppression de l'inscription
            $manager = $doctrine->getManager();
            $manager->remove($inscription);
            $manager->flush();
        }
        return $this->redirect($request->headers->get('referer'));
    }

    #[
        Route('/inscription/list/{id}', name: 'app_inscription_list'),
        IsGranted('ROLE_ENSEIGNANT')
    ]
    public function list(Sujet $sujet): Response
    {
        return $this->render('inscription/list_inscription.html.twig', [
            'controller_name' => 'SujetController',
            'data' => $sujet->getInscriptions(),
            'sujet_id' => $sujet->getId()
        ]);
    }

    #[
        Route('/inscription/{id_sujet}/{id_insc}', name: 'app_inscription_edit'),
        IsGranted('ROLE_ENSEIGNANT')
    ]
    public function edit(Request $request, EntityManagerInterface $em, int $id_sujet, int $id_insc): Response
    {
        # Récupération du sujet pour page de redirection
        $sujet = $em->getRepository(Sujet::class)->find($id_sujet);
        # Récupération de l'inscription pour le formulaire
        $insc = $em->getRepository(Inscription::class)->find($id_insc);

        $form = $this->createForm(InscriptionType::class, $insc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($insc);
            $em->flush();
            $this->addFlash('success', "Inscription enregistrée avec succès.");

            return $this->redirectToRoute('app_inscription_list', ['id' => $sujet->getId()]);
        }

        return $this->render('inscription/index.html.twig', [
            'controller_name' => 'InscriptionController',
            'inscriptionForm' => $form->createView(),
            'sujet_title' => $sujet->getTitreSujet(),
        ]);
    }

    #[
        Route('/inscription/{id}/{prenom?}/{nom?}/{email?}/{etablissement?}', name: 'app_inscription'),
        IsGranted('ROLE_ENSEIGNANT')
    ]
    # Les paramètres sont récupérés depuis le lien passé dans l'e-mail du responsable
    public function index(Request $request, EntityManagerInterface $em, MailerService $mailer, Helpers $helpers,
                          Sujet $sujet, $prenom, $nom, $email, $etablissement): Response
    {
        $insc = new Inscription();
        $form = $this->createForm(InscriptionType::class, $insc);

        #Préremplissage du formulaire
        $form->get('prenom')->setData($prenom);
        $form->get('nom')->setData($nom);
        $form->get('email')->setData($email);
        $form->get('etablissement')->setData($etablissement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # Création de l'inscription
            $insc->setDateInsc(new \DateTime());
            $insc->setSujet($sujet);
            $em->persist($insc);
            $em->flush();

            $this->addFlash('success', "Inscription enregistrée avec succès.");

            $user = $this->getUser();
            if ($user instanceof User){
                $infoResp = $helpers->getInfosRespFromUser($user);
            } else {
                $this->addFlash('error', "Erreur dans l'envoi du mail de confirmation (données de compte.");
                $this->redirectToRoute('app_home');
            }

            # Envoi du mail de confirmation à l'inscrit
            try {
                $mailer->sendInscConfirmationMail(
                    new Address($form->getData()->getEmail()),
                    [
                        'sujet_title' => $sujet->getTitreSujet(),
                        'prenom' => $infoResp->getPrenom(),
                        'nom' => $infoResp->getNom(),
                        'respo_mail' => $infoResp->getEmail(),
                    ]
                );
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', "Erreur dans l'envoi du mail de confirmation.");
                $this->redirectToRoute('app_home');
            }

            return $this->redirectToRoute('app_home');
        }

        return $this->render('inscription/index.html.twig', [
            'controller_name' => 'InscriptionController',
            'inscriptionForm' => $form->createView(),
            'sujet_title' => $sujet->getTitreSujet(),
        ]);
    }
}
