<?php

namespace App\Controller;

use App\Entity\AnneeScolaire;
use App\Entity\InfosResp;
use App\Entity\Sujet;
use App\Entity\User;
use App\Form\SujetType;
use App\Service\Helpers;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class SujetController extends AbstractController
{
    #[Route('/sujet/delete/{id}', name: 'app_sujet_delete')]
    public function delete(Request $request, ManagerRegistry $doctrine, Sujet $sujet): Response
    {
        $manager = $doctrine->getManager();

        # Suppression des inscriptions du sujet
        foreach ($sujet->getInscriptions() as $i) {
            $manager->remove($i);
        }

        #Suppression du sujet
        $manager->remove($sujet);
        $manager->flush();

        return $this->redirectToRoute('app_mes_sujets');
    }

    #[Route('/sujets', name: 'app_sujet')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $sujetRepo = $doctrine->getRepository(Sujet::class);

        $sujets = $sujetRepo->findAllFullInfo();

        # Récupération des informations pour la liste des sujets
        $data = [];
        foreach ($sujets as $s){
            $dispo = $s->getNbMaxEtu() - (sizeof($s->getInscriptions()));
            $respos = '';
            foreach ($s->getResponsables() as $r){
                $respos .= ($r->getInfos()->getPrenom().' '.$r->getInfos()->getNom().', ');
            }

            # Data à afficher
            $data[] = [
                "id" => $s->getId(),
                "titre" => $s->getTitreSujet(),
                "duree" => $s->getDureeSemaines(),
                "respos" => substr($respos, 0, -2),
                "places" => $dispo,
                "mots" => implode(",", $s->getMotCles()->toArray()),
                "filieres" => implode(",", $s->getFilieres()->toArray())];
        }

        return $this->render('sujet/index.html.twig', [
            'controller_name' => 'SujetController',
            'data' => $data
        ]);
    }

    #[Route('sujet/api/{id}', name:'api_sujet')]
    # Retourne les données pour le détail de sujet dans le modal
    public function fetchSujet(int $id, ManagerRegistry $doctrine): Response {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $sujet = $doctrine->getRepository(Sujet::class)->find($id);

        $respos = "";
        foreach ($sujet->getResponsables() as $r){
            $respos .= ($r->getInfos()->getPrenom().' '.$r->getInfos()->getNom().', ');
        }

        $data = [
            "id" => $sujet->getId(),
            "titre" => $sujet->getTitreSujet(),
            "respos" => substr($respos, 0, -2),
            "duree" => $sujet->getDureeSemaines(),
            "places" => $sujet->getNbMaxEtu() - (sizeof($sujet->getInscriptions())),
            "description" => $sujet->getDescription()
        ];

        return new JsonResponse($data);
    }

    #[
        Route('sujet/edit/{id?}', name:'app_sujet_edit'),
        IsGranted("ROLE_ENSEIGNANT")
    ]
    public function edit(Request $request, ManagerRegistry $doctrine, Sujet $sujet = null): Response {

        $new = false;
        if (!$sujet) {
            $new = true;
            $sujet = new Sujet();
        }

        $form = $this->createForm(SujetType::class, $sujet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # Insertion/Modification du sujet dans la base de données
            $sujet->setAnnee($doctrine->getRepository(AnneeScolaire::class)->findLatestYear());
            $sujet->setDateCreation(new \DateTime());
            $em = $doctrine->getManager();
            $em->persist($sujet);
            $em->flush();
            if ($new){
                $this->addFlash('success', 'Sujet créé avec succès!');
            } else {
                $this->addFlash('success', 'Sujet édité avec succès!');
            }

            return $this->redirectToRoute('app_mes_sujets');
        }

        return $this->render('sujet/creation.html.twig', [
            'controller_name' => 'SujetController',
            'sujetForm' => $form,
            'new' => $new,
            'sujet_id' => $sujet->getId()
        ]);
    }

    #[
        Route('mes_sujets', name:'app_mes_sujets'),
        IsGranted("ROLE_ENSEIGNANT")
    ]
    public function respoSujets(ManagerRegistry $doctrine, Helpers $helpers): Response {

        $sujetRepo = $doctrine->getRepository(Sujet::class);

        $user = $this->getUser();

        if ($user instanceof User){
            $infoResp = $helpers->getInfosRespFromUser($user);
        } else {
            $this->addFlash('error', 'Impossible de retrouver vos données.');
            $this->redirectToRoute('app_home');
        }

        # Récupération des sujets du responsable avec informations supplémentaires en une requête
        $sujets = $sujetRepo->findAllFullInfo($infoResp);

        $data = [];

        # Mise en forme des données pour l'affichage des sujets
        foreach ($sujets as $s){
            $respos = '';
            foreach ($s->getResponsables() as $r){
                $dispo = $s->getNbMaxEtu() - (sizeof($s->getInscriptions()));
                $respos .= ($r->getInfos()->getPrenom().' '.$r->getInfos()->getNom().', ');
            }

            $data[] = [
                "id" => $s->getId(),
                "titre" => $s->getTitreSujet(),
                "duree" => $s->getDureeSemaines(),
                "respos" => substr($respos, 0, -2),
                "places" => $dispo,
                "mots" => implode(",", $s->getMotCles()->toArray()),
                "filieres" => implode(",", $s->getFilieres()->toArray()),
                "annee" => $s->getAnnee(),
            ];
        }

        return $this->render('sujet/mes_sujets.html.twig', [
            'controller_name' => 'SujetController',
            'data' => $data,
        ]);

    }


}
