<?php

namespace App\Controller;

use App\Entity\Responsable;
use App\Entity\Sujet;
use App\Form\ContactFormType;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function index(): Response
    {
        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
        ]);
    }

    #[Route('/contact/{id}', name: 'app_contact_sujet')]
    public function register(Request $request, EntityManagerInterface $em, Sujet $sujet, MailerService $mailer): Response
    {
        $respos = $sujet->getResponsables()->toArray();

        $form = $this->createForm(ContactFormType::class, options:[
            'respos' => $respos,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $respo = $data["responsable"];
            if ($respo instanceof Responsable){
                $respoMail = $respo->getInfos()->getEmail();

                #Envoi du mail au responsable avec les informations du formulaire de contact
                try {
                    $mailer->sendContactMail(
                        new Address($respoMail),
                        [
                            'username' => 'foo',
                            'sujet_title' => $sujet->getTitreSujet(),
                            'prenom' => $data["prenom"],
                            'nom' => $data["nom"],
                            'applicant_mail' => $data["email"],
                            'etablissement' => $data["etablissement"],
                            'message' => $data["message"],
                            'sujet_id' => $sujet->getId(),
                        ]
                    );

                    $this->addFlash(
                        'success',
                        'Votre candidature a été envoyée!'
                    );

                    return $this->redirectToRoute('app_sujet');

                } catch (TransportExceptionInterface $e) {
                    // some error prevented the email sending; display an
                    // error message or try to resend the message
                    $this->addFlash(
                        'error',
                        "Erreur lors de l'envoi de la candidature!"
                    );
                }

            } else {
                $this->addFlash(
                    'error',
                    "Erreur lors de la récupération du mail du Responsable"
                );
            }
        }

        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'contactForm' => $form->createView(),
        ]);
    }
}

