<?php

namespace App\Repository;

use App\Entity\InfosResp;
use App\Entity\Responsable;
use App\Entity\Sujet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Sujet>
 *
 * @method Sujet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sujet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sujet[]    findAll()
 * @method Sujet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SujetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sujet::class);
    }

//    /**
//     * @return Sujet[] Returns an array of Sujet objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Sujet
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    # Renvoie un Sujet aléatoire
    public function randRow(): Sujet|null {
        $all = $this->findAll();
        shuffle($all);
        return $all[0];
    }

    # Renvoie toutes les InfosResp avec des informations suplémentaires en jointures
    # Si une infosResp est passé en paramètre, renvoie uniquement les informations liées à cette entité
    public function findAllFullInfo(InfosResp $infosResp = null): array {
        $qb = $this->createQueryBuilder('s');

        $qb->select("s, sr, si")
            ->leftJoin('s.responsables', 'sr')
            ->leftJoin('s.motCles', 'sm')
            ->leftJoin('s.filieres', 'sf')
            ->leftJoin('s.inscriptions', 'si')
            ;

        if ($infosResp){
            $qb->leftJoin('sr.infos', 'sri')
                ->andWhere('sri.uid = :uid')
                ->setParameter('uid', $infosResp->getUid());
        }

        return $qb->getQuery()->getResult();
    }
}
