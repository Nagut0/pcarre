<?php

namespace App\Repository;

use App\Entity\InfosResp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InfosResp>
 *
 * @method InfosResp|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfosResp|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfosResp[]    findAll()
 * @method InfosResp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfosRespRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfosResp::class);
    }

//    /**
//     * @return InfosResp[] Returns an array of InfosResp objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?InfosResp
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    # Renvoie une InfosResp aléatoire
    public function randRow(): InfosResp|null {
        $all = $this->findAll();
        shuffle($all);
        return $all[0];
    }
}
