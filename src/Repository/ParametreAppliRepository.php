<?php

namespace App\Repository;

use App\Entity\ParametreAppli;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ParametreAppli>
 *
 * @method ParametreAppli|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParametreAppli|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParametreAppli[]    findAll()
 * @method ParametreAppli[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParametreAppliRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParametreAppli::class);
    }

//    /**
//     * @return ParametreAppli[] Returns an array of ParametreAppli objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ParametreAppli
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
