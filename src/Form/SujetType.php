<?php

namespace App\Form;

use App\Entity\Filiere;
use App\Entity\MotCle;
use App\Entity\Responsable;
use App\Entity\Sujet;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SujetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre_sujet', TextType::class)
            ->add('nb_max_etu', NumberType::class)
            ->add('description', TextareaType::class)
            ->add('duree_semaines', NumberType::class)
            ->add('responsables', EntityType::class, [
                'expanded' => false,
                'class' => Responsable::class,
                'multiple' => true,
                'required' => true,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('motCles', EntityType::class, [
                'expanded' => false,
                'class' => MotCle::class,
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('filieres', EntityType::class, [
                'expanded' => false,
                'class' => Filiere::class,
                'multiple' => true,
                'required' => true,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('enregistrer', SubmitType::class, [
                'label' => 'Enregistrer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Sujet::class,
        ]);
    }
}
