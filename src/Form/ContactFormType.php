<?php

namespace App\Form;

use App\Entity\Responsable;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('responsable', EntityType::class, [
                'expanded' => false,
                'class' => Responsable::class,
                'multiple' => false,
                'required' => true,
                'choices' => $options['respos']
            ])
            ->add('prenom')
            ->add('nom')
            ->add('email', EmailType::class)
            ->add('etablissement')
            ->add('message', TextareaType::class)
            ->add('envoyer', SubmitType::class, ['label' => 'Envoyer'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
            'respos' => [],
        ]);
    }
}
